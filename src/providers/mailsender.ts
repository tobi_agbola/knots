import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Mailsender provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class Mailsender {

  constructor(public http: Http) {
    console.log('Hello Mailsender Provider');
  }
sendHireMail(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
  var type="Hire";
var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
console.log(url)
}
sendSubMail(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
   var type="Sub";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendFeedbackMail(senderEmail,SenderPhoneNo,SenderID,Subject,Body){
var url="http://www.ravcontest.com/mail-knots/feedbackmail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&Subject="+Subject+"&Body="+Body+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendRequestInpection(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
   var type="Request Inpection";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendSurveyRealtime(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
   var type="Survey Realtime";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendSetInpection(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
   var type="Set Inpection";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendRequestOtherDocument(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO,documentName){
   var type="Other Document";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"&documentname="+documentName+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendRequestThe9881000(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
   var type="9881000 Request";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
sendRequestTheq88(senderEmail,SenderPhoneNo,SenderID,shipname,shipID,ShipIMO){
   var type="q88 Request";
   var url="http://www.ravcontest.com/mail-knots/mail.php?senderemail="+senderEmail+"&senderphone="+SenderPhoneNo+"&SenderID="+SenderID+"&shipname="+shipname+"&shipID="+shipID+"&ShipIMO="+ShipIMO+"&type="+type+"";
this.http.request(url).subscribe(data => {
  console.log("mail sent")
  console.log(url)
}, err=>{console.log(err)})
}
}
