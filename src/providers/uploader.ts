import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseApp } from 'angularfire2';
/*
  Generated class for the Uploader provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class Uploader {

  constructor(public http: Http,public afDB: AngularFireDatabase, public afAuth: AngularFireAuth) {
    console.log('Hello Uploader Provider');
  }
ImageUploader(ShipId : string ,file){
  var StorageRef=firebase.storage().ref('ships_gallery/'+ file.name);
var that = this;
  var task = StorageRef.put(file);
  task.on('state_changed',
  function progress(){

  },
  function error(err){
 console.log(err.message)
 return "error occured";
  },
  function complete(){
     var UID =that.afAuth.auth.currentUser.uid;
     var DB = that.afDB.database.ref();
     var input = {"UserID": UID, "URL":task.snapshot.downloadURL,"status":"View"}
     DB.child("Ship_Gallery").child(ShipId).push(input)
    console.log("Complete");
return "Upload Complete";
  }
  );
}
}
