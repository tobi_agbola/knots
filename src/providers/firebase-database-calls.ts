import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
/*
  Generated class for the FirebaseDatabaseCalls provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FirebaseDatabaseCalls {


  constructor(public http: Http, public afDB: AngularFireDatabase, public afAuth: AngularFireAuth) {
    console.log('Hello FirebaseDatabaseCalls Provider');
  }
  public DB=this.afDB.database.ref();
  public UID=this.afAuth.auth.currentUser.uid.toString();

  AccountFirebaseInit () {
   return this.DB.child("user_card_token").child(this.UID).once('value');
  }
  AccountFirebaseDeleteCard (id) {
    return this.DB.child("user_card_token").child(this.UID).child(id).remove();
   }
}
