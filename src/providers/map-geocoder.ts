import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
//import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
/*
  Generated class for the MapGeocoder provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
 ,private nativeGeocoder: NativeGeocoder */
@Injectable()
export class MapGeocoder {
  public geocoder;
  constructor(public http: Http) {
    console.log('Hello MapGeocoder Provider');
    this.geocoder=new google.maps.Geocoder;
  }

  getLatLng(location : string){
    let geoLatLng = Observable.create( observer =>{

    this.geocoder.geocode({address:location},function(results,status){

    if(status===google.maps.GeocoderStatus.OK){

      var lat = results[0].geometry.location.lat();
      var lng = results[0].geometry.location.lng();
      var latlng = {'lat':lat,'lng':lng};
      observer.next(latlng)
      return latlng;

    }else{

      // console.log ('Geolocation failed because'+ status);

    }
    })
   })
  return geoLatLng;
  }
  getLocation(latlng){

    let geoLocation = Observable.create( observer =>{
    this.geocoder.geocode({'location':latlng},function(results,status){
    if(status===google.maps.GeocoderStatus.OK){
      var result = results[0].formatted_address
      observer.next(result);
    }else{
     // console.log ('Geolocation failed because'+ status);
    }
    }) })
    return geoLocation;
  }

  returnLatLng(location : string){
    this.getLatLng(location)
  }
  getShipPath(lastportArray,currentLocationArray,destinationArray){
    var ShipPath=[];
    if(currentLocationArray==destinationArray){

        ShipPath.push(lastportArray);
        ShipPath.push(destinationArray);

    }else if(lastportArray==currentLocationArray){

      ShipPath.push(currentLocationArray);
      ShipPath.push(destinationArray);

    }else{

      ShipPath.push(lastportArray);
      ShipPath.push(currentLocationArray);
      ShipPath.push(destinationArray);

    }
    //console.log(ShipPath)
    return ShipPath;
  }
}
