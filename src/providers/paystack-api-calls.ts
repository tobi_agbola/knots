import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import * as firebase from 'firebase/app';
/*
  Generated class for the PaystackApiCalls provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PaystackApiCalls {
  public DB=this.afDB.database.ref();
  public currentTime= new Date().getTime();

  constructor(public http: Http, public afDB: AngularFireDatabase, public afAuth: AngularFireAuth) {
    console.log('Hello PaystackApiCalls Provider');
  }
  VerifyTransationRef( reference ): Observable<object>{
  console.log(reference);
  var that = this;
  var responseStatus : boolean;
  var responseObject;
  var responseLast4;
  var responseCardKey;
  var UID=this.afAuth.auth.currentUser.uid.toString();
  var url="http://www.ravcontest.com/user-reference-knots/verify-reference.php?ref="+reference+"";

  return Observable.create(function(observer) {

    that.http.request(url).subscribe((data)=>{

      console.log(url);

      console.log(data.json());

      if(data.ok && data.json().message=="Verification successful"){

        var InputCard = {authorization_code: data.json().data.authorization.authorization_code,  bin: data.json().data.authorization.bin,
          last4: data.json().data.authorization.last4, exp_month: data.json().data.authorization.exp_month, exp_year: data.json().data.authorization.exp_year,
          customer_code: data.json().data.customer.customer_code ,customer_email:  data.json().data.customer.email, created_at:that.currentTime, last_login:that.currentTime};

        that.DB.child("user_card_token").child(UID).once('value').then((snapshot)=>{

          if(snapshot.val()){

          var keys = Object.keys(snapshot.val());

          var cardPush;

          for (let key of keys) {

            if(snapshot.val()[key].last4 == data.json().data.authorization.last4){

              responseStatus = false;

              responseObject = {status:responseStatus , error:" Card Already Exists"};

              observer.next(responseObject);

              break;

            }else{
              cardPush = that.DB.child("user_card_token").child(UID).push();

              responseCardKey = cardPush.key;

              responseStatus = true;

              cardPush.set(InputCard);

              responseLast4 = data.json().data.authorization.last4;

              responseObject = {status:responseStatus, last4: responseLast4, key: responseCardKey};

              observer.next(responseObject);

            }
          }
          }else{
            cardPush = that.DB.child("user_card_token").child(UID).push();

            responseCardKey = cardPush.key;

            responseStatus = true;

            cardPush.set(InputCard);

            responseLast4 = data.json().data.authorization.last4;

            responseObject = {status:responseStatus, last4: responseLast4, key: responseCardKey};

            observer.next(responseObject);

          }

        }).catch((err)=>{

            responseStatus = false;

            responseObject = {status:responseStatus , error:err};

            observer.next(responseObject);

        })
        }

      },(err)=>{

          responseStatus = false;

          responseObject = {status:responseStatus , error:err};

          observer.next(responseObject);

      });

      //observer.complete();

  });



  }
  RenewUserSubscription(SubscriptionStatus){
    var that = this;

    var UID=this.afAuth.auth.currentUser.uid.toString();

    this.DB.child("user_card_token").child(UID).once("value").then((snapshot)=>{

    }).catch((err)=>{

      console.log(err);

    })
  }

  recurrentBilling(authoriationCode, email, amount){
    var that = this;
    var url="http://www.ravcontest.com/user-reference-knots/recurrentbill.php?authorization_code="+authoriationCode+"&email="+email+"&amount="+amount+"";
    return Observable.create(function(observer){
      that.http.request(url).subscribe((data)=>{
        console.log(data.json());
        observer.next(data.json());
      })
    })
  }
}
