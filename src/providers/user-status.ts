import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Login } from '../pages/login/login';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

/*
  Generated class for the UserStatus provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserStatus {

  constructor(public http: Http,public afAuth: AngularFireAuth, public afDB: AngularFireDatabase ) {
    console.log('Hello UserStatus Provider');
  }
  CheckLogin(){

  }
   GetUserInfo(){
    //Gets and stores users infomation in localstorage
   console.log(this.afAuth.auth.currentUser.uid.toString())
   var DB=this.afDB.database.ref().child("Users");
   DB.child(this.afAuth.auth.currentUser.uid.toString()).once('value').then(data=>{
     console.log(data.val())
     if (typeof(Storage) !== "undefined") {
       if(localStorage.UID==undefined){
    localStorage.setItem("UID",this.afAuth.auth.currentUser.uid.toString())
    localStorage.setItem("country",data.val().country)
    localStorage.setItem("company",data.val().company)
    localStorage.setItem("firstname",data.val().firstname)
    localStorage.setItem("lastname",data.val().lastname)
    localStorage.setItem("status", data.val().status)
    localStorage.setItem("phoneno",data.val().phoneno)
    
  }

} else {
    console.log("Error No Webstorage Support")
}
   })
  }
}
