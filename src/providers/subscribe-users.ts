import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';

/*
  Generated class for the SubscribeUsers provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SubscribeUsers {

  public userEmail = this.afAuth.auth.currentUser.email.toString();
  public currentTime= new Date().getTime();
  public nowDate = new Date();
  public year = this.nowDate.getFullYear();
  public month = this.nowDate.getMonth();
  public day = this.nowDate.getDate();
  public datePlusYear = new Date(this.year + 1, this.month, this.day).getTime();
  public datePlusMonth = new Date(this.year, this.month + 1, this.day).getTime();
  public DB=this.afDB.database.ref();
  public stopTime;

  constructor(public http: Http, public afDB: AngularFireDatabase, public afAuth: AngularFireAuth) {
    console.log('Hello SubscribeUsers Provider');
  }

  subscribeBasicYearly(event) {

    if(sessionStorage.getItem("Stop_Time")!=undefined){

      if(this.currentTime<=parseInt(sessionStorage.getItem("Stop_Time"))){

        this.stopTime = this.datePlusYear + (parseInt(sessionStorage.getItem("Stop_Time"))-this.currentTime);

      }else{

        this.stopTime = this.datePlusYear;

      }

    }else{

      this.stopTime = this.datePlusYear;

    }

    var Input = {status:"Basic Yearly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    var InputLog = {UID : sessionStorage.getItem("UID"),referenceNo:event.reference,Cost:50000, Subscription:"Basic Yearly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    this.DB.child("Users").child(sessionStorage.getItem("UID")).update(Input);

    this.DB.child("Subscription_log").push().set(InputLog);

    sessionStorage.setItem("status","Basic Yearly");

    sessionStorage.setItem("Stop_Time",""+this.stopTime+"");

  }

  subscribePremiumYearly(event) {

    if(sessionStorage.getItem("Stop_Time")!=undefined){

      if(this.currentTime<=parseInt(sessionStorage.getItem("Stop_Time"))){

        this.stopTime = this.datePlusYear + (parseInt(sessionStorage.getItem("Stop_Time"))-this.currentTime);

      }else{

        this.stopTime = this.datePlusYear;

      }

    }else{

      this.stopTime = this.datePlusYear;

    }

    var Input = {status:"Premium Yearly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    var InputLog = {UID : sessionStorage.getItem("UID"),referenceNo:event.reference,Cost:50000, Subscription:"Premium Yearly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    this.DB.child("Users").child(sessionStorage.getItem("UID")).update(Input);

    this.DB.child("Subscription_log").push().set(InputLog);

    sessionStorage.setItem("status","Premium Yearly");

    sessionStorage.setItem("Stop_Time",""+this.stopTime+"");

  }

  subscribeBasicMonthly(event) {

    if(sessionStorage.getItem("Stop_Time")!=undefined){

      if(this.currentTime<=parseInt(sessionStorage.getItem("Stop_Time"))){

        this.stopTime = this.datePlusMonth + (parseInt(sessionStorage.getItem("Stop_Time"))-this.currentTime);

      }else{

        this.stopTime = this.datePlusMonth;

      }

    }else{

      this.stopTime = this.datePlusMonth;

    }

    var Input = {status:"Basic Monthly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    var InputLog = {UID : sessionStorage.getItem("UID"),referenceNo:event.reference,Cost:50000, Subscription:"Basic Monthly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    this.DB.child("Users").child(sessionStorage.getItem("UID")).update(Input);

    this.DB.child("Subscription_log").push().set(InputLog);

    sessionStorage.setItem("status","Basic Monthly");

    sessionStorage.setItem("Stop_Time",""+this.stopTime+"");

  }

  subscribePremiumMonthly(event){

    if(sessionStorage.getItem("Stop_Time")!=undefined){

      if(this.currentTime<=parseInt(sessionStorage.getItem("Stop_Time"))){

        this.stopTime = this.datePlusMonth + (parseInt(sessionStorage.getItem("Stop_Time"))-this.currentTime);

      }else{

        this.stopTime = this.datePlusMonth;

      }

    }else{

      this.stopTime = this.datePlusMonth;

    }

    var Input = {status:"Premium Yearly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    var InputLog = {UID : sessionStorage.getItem("UID"),referenceNo:event.reference,Cost:50000, Subscription:"Premium Yearly",Start_Time:this.currentTime,Stop_Time:this.stopTime};

    this.DB.child("Users").child(sessionStorage.getItem("UID")).update(Input);

    this.DB.child("Subscription_log").push().set(InputLog);

    sessionStorage.setItem("status","Premium Monthly");

    sessionStorage.setItem("Stop_Time",""+this.stopTime+"");

  }
}
