import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Home} from '../../pages/home/home'
import {Profile} from '../../pages/profile/profile'
import {Settings} from '../../pages/settings/settings'
/**
 * Generated class for the Tabs tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class Tabs {

  tab1Root: any = Home;
  tab2Root: any = Profile;
  tab3Root: any = Settings;

  constructor(public navCtrl: NavController) {}

}
