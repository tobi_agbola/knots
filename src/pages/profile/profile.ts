import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Settings } from '../../pages/settings/settings';
/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class Profile implements OnInit {
public Profile={};
  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase ) {

  }
ngOnInit(){
this.ProfileDetails();
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile');
  }
ProfileDetails(){
   var DB=this.afDB.database.ref().child("Users");
   var that = this;
   DB.child(this.afAuth.auth.currentUser.uid.toString()).once('value').then(data=>{
     console.log(data.val());
     var startDate = new Date(parseInt(sessionStorage.getItem('Start_Time')));
     var stopDate = new Date(parseInt(sessionStorage.getItem('Stop_Time')));
   if(sessionStorage.UID==undefined){
    sessionStorage.setItem("UID",this.afAuth.auth.currentUser.uid.toString());
    sessionStorage.setItem("country",data.val().country);
    sessionStorage.setItem("company",data.val().company);
    sessionStorage.setItem("firstname",data.val().firstname);
    sessionStorage.setItem("lastname",data.val().lastname);
    sessionStorage.setItem("status", data.val().status);
    sessionStorage.setItem("Start_Time", data.val().Start_Time);
    sessionStorage.setItem("Stop_Time", data.val().Stop_Time);
    sessionStorage.setItem("phoneno",data.val().phoneno);
  this.Profile['firstname']=sessionStorage.getItem('firstname');
  this.Profile['lastname']=sessionStorage.getItem('lastname');
  this.Profile['country']=sessionStorage.getItem('country');
  this.Profile['company']=sessionStorage.getItem('company');
  this.Profile['status']=sessionStorage.getItem('status');
  this.Profile['email']=that.afAuth.auth.currentUser.email;
  this.Profile['Start_Time']= startDate.toDateString();
  this.Profile['Stop_Time']= stopDate.toDateString();
    console.log(sessionStorage.getItem("phoneno"));
  }else{
  this.Profile['firstname']=sessionStorage.getItem('firstname');
  this.Profile['lastname']=sessionStorage.getItem('lastname');
  this.Profile['country']=sessionStorage.getItem('country');
  this.Profile['company']=sessionStorage.getItem('company');
  this.Profile['status']=sessionStorage.getItem('status');
  this.Profile['phoneno']=sessionStorage.getItem('phoneno');
  this.Profile['Start_Time']= startDate.toDateString();
  this.Profile['Stop_Time']= stopDate.toDateString();
  this.Profile['email']=that.afAuth.auth.currentUser.email;
  }
  console.log(sessionStorage.getItem('Start_Time'));
  console.log(sessionStorage.getItem('Stop_Time'));
   });
  console.log(sessionStorage.status);
}
goToSettings(){

  this.navCtrl.push(Settings);

}
}
