import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingOrders } from './shipping-orders';

@NgModule({
  declarations: [
    ShippingOrders,
  ],
  imports: [
    IonicPageModule.forChild(ShippingOrders),
  ],
  exports: [
    ShippingOrders
  ]
})
export class ShippingOrdersModule {}
