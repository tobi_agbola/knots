import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { SimulateShips } from '../../providers/simulate-ships';
import { UserStatus  } from '../../providers/user-status';
import { Mailsender } from '../../providers/mailsender';
import {MapGeocoder} from '../../providers/map-geocoder';
import * as firebase from 'firebase/app';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { ModalController } from 'ionic-angular';
import { Imageslide } from '../../components/imageslide/imageslide';
import { Login } from '../../pages/login/login';
import { ShipInformationPayInfo } from '../../components/ship-information-pay-info/ship-information-pay-info';

/**
 * Generated class for the ShippingOrders page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-shipping-orders',
  templateUrl: 'shipping-orders.html',
})
export class ShippingOrders {
  public id = this.navParams.get('Ship_Id');
public userStatus;
public senderEmail;
public shipInfo;
public senderID;
public shipsView = "Vessel";
public currentTime= new Date().getTime();
public DB =  this.afDB.database.ref();

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase, public loadingCtrl: LoadingController, public ShipSimulation: SimulateShips,public mapGeocoder: MapGeocoder, public mailSender: Mailsender,private alertCtrl: AlertController,public user: UserStatus) {
    
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShippingOrders');
    
  }

 Testfunction(){
   console.log("messi");
   
 }

}
