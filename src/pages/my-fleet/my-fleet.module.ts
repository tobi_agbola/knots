import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyFleet } from './my-fleet';

@NgModule({
  declarations: [
    MyFleet,
  ],
  imports: [
    IonicPageModule.forChild(MyFleet),
  ],
  exports: [
    MyFleet
  ]
})
export class MyFleetModule {}
