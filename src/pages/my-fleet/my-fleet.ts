import { Component, OnInit  } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { FirebaseApp } from 'angularfire2';
import {ShipInformation} from '../../pages/ship-information/ship-information';
/**
 * Generated class for the MyFleet page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-fleet',
  templateUrl: 'my-fleet.html',
})
export class MyFleet implements OnInit {
public fleetData=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyFleet');
  }
ngOnInit(){
this.LoadFleet();
}
LoadFleet(){
var DB =this.afDB.database.ref().child("userFleet");
var userID =  this.afAuth.auth.currentUser.uid;
DB.child(userID).once('value', (snapshot)=>{
for(let key in snapshot.val()){
console.log(snapshot.val()[key].id);
console.log(snapshot.val()[key].name);
this.fleetData.push(snapshot.val()[key])
}
})
}
ShipInformation(Ship_Id){
    this.navCtrl.push(ShipInformation,{
      "Ship_Id" : Ship_Id
    })
}
deleteShip(Ship_id){
let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  loading.present();
var DB =this.afDB.database.ref().child("userFleet");
var userID =  this.afAuth.auth.currentUser.uid;
DB.child(userID).once('value', (snapshot)=>{
for(let key in snapshot.val()){
if(snapshot.val()[key].id===Ship_id){
DB.child(userID).child(key).remove();
this.fleetData=[];
this.LoadFleet();
}
}
})
  loading.dismiss();
}
}
