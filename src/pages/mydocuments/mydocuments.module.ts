import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mydocuments } from './mydocuments';

@NgModule({
  declarations: [
    Mydocuments,
  ],
  imports: [
    IonicPageModule.forChild(Mydocuments),
  ],
  exports: [
    Mydocuments
  ]
})
export class MydocumentsModule {}
