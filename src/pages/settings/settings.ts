import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Angular4PaystackModule } from 'angular4-paystack';
//import { Angular4PaystackComponent } from 'angular4-paystack';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Login } from '../../pages/login/login';
import * as firebase from 'firebase/app';
import { CardChoiceComponent } from '../../components/card-choice-component/card-choice-component';

/**
 * Generated class for the Settings page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class Settings {
  public settingsView='editProfile';
  public FormData={};
  public FormPassword={};
  public UID;
  public userEmail = this.afAuth.auth.currentUser.email.toString();
  public refNo=Math.floor(100000000 + Math.random() * 900000000) + sessionStorage.getItem("UID");
  public currentTime= new Date().getTime();
  public DB=this.afDB.database.ref();
  public btnBasicMonthly=true;
  public btnBasicYearly=true;
  public btnPremiumMonthly=true;
  public btnPremiumYearly=true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public PayStack: Angular4PaystackModule, public afDB: AngularFireDatabase, public afAuth: AngularFireAuth, public modalCtrl: ModalController) {

    this.setFormVariables();

    this.statusObserver();

    this.setButton();

  }
  statusObserver() {

    var that = this;

    firebase.auth().onAuthStateChanged(function(user) {

    if (user) {

    } else {

    that.navCtrl.setRoot(Login);

    }

    });

  }
  setButton(){
    if(sessionStorage.getItem("status")){
      if(sessionStorage.getItem("status")=="Basic Yearly"){

        this.btnBasicYearly=false;

      }else if(sessionStorage.getItem("status")=="Basic Monthly"){

        this.btnBasicMonthly=false;
        
      }else if(sessionStorage.getItem("status")=="Premium Yearly"){

        this.btnPremiumYearly=false;
        
      }else if(sessionStorage.getItem("status")=="Premium Monthly"){
        
        this.btnPremiumMonthly=false;

      }
    }
  }
  updateRegisterForm(){
    console.log(this.FormData);

    var UID=this.afAuth.auth.currentUser.uid.toString();

    var userInput ={firstname:this.FormData['firstname'], lastname:this.FormData['lastname'],country:this.FormData['country'],company:this.FormData['company'],phoneno:this.FormData['phoneno'],updated_at:this.currentTime};

                  console.log(userInput);

    this.afDB.database.ref().child("Users").child(UID).update(userInput).then(data =>{

      sessionStorage.setItem("UID",this.afAuth.auth.currentUser.uid.toString())

      sessionStorage.setItem("country",this.FormData['country']);

      sessionStorage.setItem("company",this.FormData['company']);

      sessionStorage.setItem("first",this.FormData['firstname']);

      sessionStorage.setItem("lastname",this.FormData['lastname']);

      sessionStorage.setItem("phoneno",this.FormData['phoneno']);

      this.navCtrl.popToRoot();

    }).catch(err=>{ console.log(err);})
  }

  setFormVariables(){

    var UID=this.afAuth.auth.currentUser.uid.toString();

    this.afDB.database.ref().child("Users").child(UID).once("value").then(data => {

    this.FormData['country']=data.val().country;

    this.FormData['firstname']=data.val().firstname;

    this.FormData['lastname']=data.val().lastname;

    this.FormData['company']=data.val().company;

    this.FormData['phoneno']=data.val().phoneno;

    console.log(data.val());

    }).catch(err =>{console.log(err.message);})

  }

  passwordForm(){
    if(this.FormPassword['newPassword']===this.FormPassword['retypenewpassword']){

    this.afAuth.auth.signInWithEmailAndPassword(this.afAuth.auth.currentUser.email.toString(),this.FormPassword['oldPassword']).then(data =>{

    this.afAuth.auth.currentUser.updatePassword(this.FormPassword['newPassword']).then(data =>{

    this.navCtrl.popToRoot();

    }).catch(err=>{console.log(err.message);})

    }).catch(err=>{console.log(err.message);})

    }
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad Settings');

  }

  setRootHome(){

    this.navCtrl.popToRoot();

  }

  getPaymentChoiceModal(userChoice) {

    const paymentChoiceModalPop = this.modalCtrl.create( CardChoiceComponent , { packageType : userChoice.packageType , packageDuration : userChoice.packageDuration , packageCost : userChoice.packageCost});

    paymentChoiceModalPop.present();

    paymentChoiceModalPop.onDidDismiss(() => {

      if(sessionStorage.getItem("status")=="Basic Yearly"){

        this.setRootHome();

      }else if(sessionStorage.getItem("status")=="Basic Monthly") {

        this.setRootHome();

      }else if(sessionStorage.getItem("status")=="Premium Yearly") {

        this.setRootHome();

      }else if(sessionStorage.getItem("status")=="Premium Monthly") {

        this.setRootHome();

      }

    })

  }
}
