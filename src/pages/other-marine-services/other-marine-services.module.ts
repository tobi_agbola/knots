import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherMarineServices } from './other-marine-services';

@NgModule({
  declarations: [
    OtherMarineServices,
  ],
  imports: [
    IonicPageModule.forChild(OtherMarineServices),
  ],
  exports: [
    OtherMarineServices
  ]
})
export class OtherMarineServicesModule {}
