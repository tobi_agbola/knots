import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { ShipToshipCompanies } from '../../pages/ship-toship-companies/ship-toship-companies';
import { ShippingAgencies } from '../../pages/shipping-agencies/shipping-agencies';

/**
 * Generated class for the OtherMarineServices page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-other-marine-services',
  templateUrl: 'other-marine-services.html',
})
export class OtherMarineServices {

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherMarineServices');
  }

  gotoShipToshipCompanies(){

    this.navCtrl.push(ShipToshipCompanies);

    this.menuCtrl.close();

  }
  gotoShippingAgencies(){

    this.navCtrl.push(ShippingAgencies);

    this.menuCtrl.close();

  }

}
