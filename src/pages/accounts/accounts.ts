import { Component , ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FirebaseDatabaseCalls } from '../../providers/firebase-database-calls';
import { Angular4PaystackModule } from 'angular4-paystack';
import { AngularFireAuth } from 'angularfire2/auth';
import { PaystackApiCalls } from '../../providers/paystack-api-calls';
/**
 * Generated class for the Accounts page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html',
})
export class Accounts {
public Cards;
public CardsKeys;
public message;
public newCardHtml='';
public loader;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public firebaseDatabase : FirebaseDatabaseCalls, public PayStack: Angular4PaystackModule, public afAuth: AngularFireAuth, public PayStackAPI: PaystackApiCalls, private elementRef: ElementRef, public loadingCtrl: LoadingController) {
  }
  public userEmail = this.afAuth.auth.currentUser.email.toString();
  public refNo=Math.floor(100000000 + Math.random() * 900000000) + sessionStorage.getItem("UID");
  ionViewDidLoad() {
    console.log('ionViewDidLoad Accounts');
    this.LoadCards();
  }
  LoadCards(){
    var that = this;
    this.firebaseDatabase.AccountFirebaseInit().then(snapshot => {
      that.CardsKeys = Object.keys(snapshot.val());
      that.Cards = snapshot.val();
      console.log(snapshot.val());
    }).catch(err =>{
      that.message="empty";
      console.log(err);
    });
  }
  DeleteCard(id){
   var that = this;
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure you want to delete this card',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
           console.log("Cancelled")
          }
        },
        {
          text: 'Delete',
          handler: () => {
            document.getElementById(id).style.display= "none";
            that.refNo=Math.floor(100000000 + Math.random() * 900000000) + sessionStorage.getItem("UID");
            that.firebaseDatabase.AccountFirebaseDeleteCard(id).then(data =>{
            })
          }
        }
      ]
    });
    alert.present();
    console.log(id);
  }
  paymentCancel(){

    this.stopLoader();

  }
  addCard(event){
    var element = this.elementRef.nativeElement.querySelector('#newCard');
    var that = this;
    this.PayStackAPI.VerifyTransationRef(event.reference).subscribe((data) => {
      that.stopLoader();

      console.log(data);

      if(data["status"]){

         //this.newCardHtml=this.newCardHtml + '<ion-item id="'+data["key"]+'"><ion-icon ios="ios-card" md="md-card" item-start></ion-icon>'+data["last4"]+'<ion-icon ios="ios-close" md="md-close" (click)="DeleteCard('+data["key"]+')" item-end></ion-icon></ion-item>';

         let alert = this.alertCtrl.create({
          title: 'Card '+ data["last4"]+' added Successfully',
          subTitle: "Success",
          buttons: [
            {
              text: 'Ok',
              role: 'Ok',
              handler: () => {
                that.navCtrl.popToRoot();
              }
            }
          ]
        });
        alert.present();

      }else{

        let alert = this.alertCtrl.create({
          title: 'Error Adding Card',
          subTitle: data["error"],
          buttons: ['Dismiss']
        });
        alert.present();

      }
    });
  }
  startLoader(){

    this.loader = this.loadingCtrl.create({

      content: 'Please wait...'

    });

    this.loader.present();

  }

  stopLoader(){

    this.loader.dismiss();

  }
}
