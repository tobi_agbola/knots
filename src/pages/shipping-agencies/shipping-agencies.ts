import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { CompaniesContactInfo } from '../../components/companies-contact-info/companies-contact-info';

/**
 * Generated class for the ShippingAgencies page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-shipping-agencies',
  templateUrl: 'shipping-agencies.html',
})
export class ShippingAgencies {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShippingAgencies');
  }
  viewAgencyContactDetails(){
    const CompaniesContactInfoModal = this.modalCtrl.create(CompaniesContactInfo, { companyID : 8675309, companyType : 8675309, companyName : "Knots NG" , companyEmail : "info@knotsng.com" , companyNumber : 8019042803 , companyAddress : "Mobolaji Johnson Estate Lekki Lagos" , companyLogo : "http://knotsng.com/assets/img/logo/site-logo.svg", companyDescription:"STS dskjsdnksf;vwenroepomve"  });
    CompaniesContactInfoModal.present();
  }
}
