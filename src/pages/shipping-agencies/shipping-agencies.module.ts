import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingAgencies } from './shipping-agencies';

@NgModule({
  declarations: [
    ShippingAgencies,
  ],
  imports: [
    IonicPageModule.forChild(ShippingAgencies),
  ],
  exports: [
    ShippingAgencies
  ]
})
export class ShippingAgenciesModule {}
