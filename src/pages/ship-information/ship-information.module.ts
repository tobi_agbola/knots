import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShipInformation } from './ship-information';

@NgModule({
  declarations: [
    ShipInformation,
  ],
  imports: [
    IonicPageModule.forChild(ShipInformation),
  ],
  exports: [
    ShipInformation
  ]
})
export class ShipInformationModule {}
