import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController  } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { SimulateShips } from '../../providers/simulate-ships';
import { UserStatus  } from '../../providers/user-status';
import { Mailsender } from '../../providers/mailsender';
import {MapGeocoder} from '../../providers/map-geocoder';
import { Packages } from '../../pages/packages/packages';
import { Login } from '../../pages/login/login';
import * as firebase from 'firebase/app';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { ModalController } from 'ionic-angular';
import { Imageslide } from '../../components/imageslide/imageslide';
import { ShipInformationPayInfo } from '../../components/ship-information-pay-info/ship-information-pay-info';
import { Uploader } from '../../providers/uploader';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the ShipInformation page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-ship-information',
  templateUrl: 'ship-information.html',
})
export class ShipInformation implements OnInit {

public id = this.navParams.get('Ship_Id');
public shipsView="Vessel";
public userStatus;
public ShipName;
public ShipId;
public currentLocation;
public ShipLength;
public ShipWidth;
public TypeName;
public YearBuilt;
public Destination;
public ShipIMO;
public senderEmail;
public MMSI;
public IMO;
public LONG;
public Lat;
public Speed;
public DESTINATION;
public AIS_TYPE_SUMMARY;
public ETA;
public CURRENT_PORT;
public LAST_PORT_TIME;
public CURRENT_PORT_COUNTRY;
public CURRENT_PORT_ID;
public CURRENT_PORT_UNLOCODE;
public LAST_PORT_COUNTRY;
public LAST_PORT_UNLOCODE;
public LAST_PORT_NAME;
public LAST_PORT_ID;
public NEXT_PORT_ID;
public NEXT_PORT_UNLOCODE;
public NEXT_PORT_NAME;
public NEXT_PORT_COUNTRY;
public ETA_CALC;
public ETA_UPDATED;
public DISTANCE_TO_GO;
public DISTANCE_TRAVELLED;
public AVG_SPEED;
public MAX_SPEED;
public COURSE;
public senderID;
public FLAG;
public currentTime= new Date().getTime();

  constructor(public navCtrl: NavController, public navParams: NavParams, public camera:Camera,private photoLibrary: PhotoLibrary,public modalCtrl: ModalController, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase, public loadingCtrl: LoadingController, public ShipSimulation: SimulateShips,public Uploader: Uploader,public mapGeocoder: MapGeocoder, public mailSender: Mailsender,private alertCtrl: AlertController,public user: UserStatus  ) {


}
ngOnInit(){
  this.statusObserver();
  this.getShipInformation(this.id)
}
 // public senderEmail=this.afAuth.auth.currentUser.email;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShipInformation');
  }
 
  getShipInformation(Ship_id){

    let loading = this.loadingCtrl.create({

    content: 'Please wait...'

    });
    loading.present();

    var ShipDatabase=this.afDB.database.ref().child('Ships').child(Ship_id);

    ShipDatabase.once('value').then(data => {

      if(data.val()== null){

        this.insertApiInformation(Ship_id);

      }else{

        this.updateShipInformation(Ship_id);

      }
    })
    loading.dismiss();

  }

  statusObserver(){
    var that = this;
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        that.user.GetUserInfo();
        that.userStatus=true;
        that.senderEmail=that.afAuth.auth.currentUser.email;
      if(sessionStorage.getItem("status")=="waiting"){
        that.getPackages();
      }
        //that.menuCtrl.enable(false, 'unauthenticated');
      } else {
        that.userStatus=false;
        //that.menuCtrl.enable(false, 'authenticated');
        // that.navCtrl.setRoot(Login);
      }
    });
  }
  insertApiInformation(Ship_Id){
    this.ShipSimulation.getShips().subscribe(data => {
    if(data.SHIP_ID==Ship_Id){
      var DB=this.afDB.database.ref().child("Ships");
      var shipInput ={IMO:data.IMO,SHIPNAME:data.SHIPNAME,SHIPTYPE:data.SHIPTYPE,LENGTH:data.LENGTH,WIDTH:data.WIDTH,TYPE_NAME:data.TYPE_NAME
      ,AIS_TYPE_SUMMARY:data.AIS_TYPE_SUMMARY,YEAR_BUILT:data.YEAR_BUILT,created_at:this.currentTime,updated_at:this.currentTime}
      this.mapGeocoder.getLocation({lat:data.LAT,lng:data.LON}).subscribe( data => {
        this.currentLocation=data;
      })
        console.log(data);
        this.MMSI=data.MMSI;
        this.ShipName=data.SHIPNAME;
        this.ShipLength=data.LENGTH;
        this.ShipWidth=data.WIDTH;
        this.TypeName=data.TYPE_NAME;
        this.YearBuilt=data.YEAR_BUILT;
        this.Destination=data.DESTINATION;
        this.ShipIMO=data.IMO;
        this.AIS_TYPE_SUMMARY=data.AIS_TYPE_SUMMARY;
        this.AVG_SPEED=data.AVG_SPEED;
        this.MAX_SPEED=data.MAX_SPEED;
        this.CURRENT_PORT=data.CURRENT_PORT;
        this.CURRENT_PORT_COUNTRY=data.CURRENT_PORT_COUNTRY;
        this.CURRENT_PORT_ID=data.CURRENT_PORT_ID;
        this.CURRENT_PORT_UNLOCODE=data.CURRENT_PORT_UNLOCODE;
        this.DISTANCE_TO_GO=data.DISTANCE_TO_GO;
        this.ETA=data.ETA;
        this.ETA_CALC=data.ETA_CALC;
        this.ETA_UPDATED=data.ETA_UPDATED;
        this.LAST_PORT_ID=data.LAST_PORT_ID;
        this.LAST_PORT_NAME=data.LAST_PORT;
        this.LAST_PORT_COUNTRY=data.LAST_PORT_COUNTRY;
        this.LAST_PORT_TIME=data.LAST_PORT_TIME;
        this.LAST_PORT_UNLOCODE=data.LAST_PORT_UNLOCODE;
        this.NEXT_PORT_COUNTRY=data.NEXT_PORT_COUNTRY;
        this.NEXT_PORT_ID=data.NEXT_PORT_ID;
        this.NEXT_PORT_UNLOCODE=data.NEXT_PORT_UNLOCODE;
        this.NEXT_PORT_NAME=data.NEXT_PORT_NAME;
        this.LONG=data.LON;
        this.Lat=data.LAT;
        this.COURSE=data.COURSE;
        this.FLAG=data.FLAG;
        DB.child(data.SHIP_ID).set(shipInput);
    }
    })
  }

   GetUserInfo(){
    //Gets and stores users infomation in sessionStorage
    console.log(this.afAuth.auth.currentUser.uid.toString());
      var DB=this.afDB.database.ref().child("Users");
    DB.child(this.afAuth.auth.currentUser.uid.toString()).once('value').then(data=>{

      console.log(data.val());
    if (typeof(Storage) !== "undefined") {

      if(sessionStorage.UID==undefined){

        sessionStorage.setItem("UID",this.afAuth.auth.currentUser.uid.toString())
        sessionStorage.setItem("country",data.val().country)
        sessionStorage.setItem("company",data.val().company)
        sessionStorage.setItem("firstname",data.val().firstname)
        sessionStorage.setItem("lastname",data.val().lastname)
        sessionStorage.setItem("status", data.val().status)
        sessionStorage.setItem("phoneno",data.val().phoneno)

      }
      if(sessionStorage.getItem("status")=="waiting"){

        this.getPackages();
      
      }

    } else {
      console.log("Error No Webstorage Support");

      if(sessionStorage.getItem("status")=="waiting"){

        this.getPackages();

      }
    }
    });
  }

  getPackages(){
    
    //Moves view to packages page
    this.navCtrl.setRoot(Packages);

  }
  updateShipInformation(Ship_Id){

    this.ShipSimulation.getShips().subscribe(data => {

    var DB=this.afDB.database.ref().child("Ships");

    DB.child(Ship_Id).once('value').then(snapshot =>{

      this.ShipName=snapshot.val().SHIPNAME;
      this.ShipLength=snapshot.val().LENGTH;
      this.ShipWidth=snapshot.val().WIDTH;
      this.TypeName=snapshot.val().TYPE_NAME;
      this.YearBuilt=snapshot.val().YEAR_BUILT;
      this.ShipIMO=snapshot.val().IMO

    })

    if(data.SHIP_ID==Ship_Id){
       console.log(data);
      var shipInput ={YEAR_BUILT:data.YEAR_BUILT,CURRENT_LAT:data.LAT, CURRENT_LON:data.LON,CURRENT_DESTINATION:data.DESTINATION}
      //alert({lat:data.LAT,lng:data.LON})
      this.mapGeocoder.getLocation({lat:data.LAT,lng:data.LON}).subscribe( locationData => {
      this.currentLocation=locationData;
      })
      this.MMSI=data.MMSI;
      this.ShipName=data.SHIPNAME;
      this.ShipLength=data.LENGTH;
      this.ShipWidth=data.WIDTH;
      this.TypeName=data.TYPE_NAME;
      this.YearBuilt=data.YEAR_BUILT;
      this.Destination=data.DESTINATION;
      this.ShipIMO=data.IMO;
      this.AIS_TYPE_SUMMARY=data.AIS_TYPE_SUMMARY;
      this.AVG_SPEED=data.AVG_SPEED;
      this.MAX_SPEED=data.MAX_SPEED;
      this.CURRENT_PORT=data.CURRENT_PORT;
      this.CURRENT_PORT_COUNTRY=data.CURRENT_PORT_COUNTRY;
      this.CURRENT_PORT_ID=data.CURRENT_PORT_ID;
      this.CURRENT_PORT_UNLOCODE=data.CURRENT_PORT_UNLOCODE;
      this.DISTANCE_TO_GO=data.DISTANCE_TO_GO;
      this.ETA=data.ETA;
      this.ETA_CALC=data.ETA_CALC;
      this.ETA_UPDATED=data.ETA_UPDATED;
      this.LAST_PORT_ID=data.LAST_PORT_ID;
      this.LAST_PORT_NAME=data.LAST_PORT;
      this.LAST_PORT_COUNTRY=data.LAST_PORT_COUNTRY;
      this.LAST_PORT_TIME=data.LAST_PORT_TIME;
      this.LAST_PORT_UNLOCODE=data.LAST_PORT_UNLOCODE;
      this.NEXT_PORT_COUNTRY=data.NEXT_PORT_COUNTRY;
      this.NEXT_PORT_ID=data.NEXT_PORT_ID;
      this.NEXT_PORT_UNLOCODE=this.NEXT_PORT_UNLOCODE;
      this.NEXT_PORT_NAME=data.NEXT_PORT_NAME;
      this.LONG=data.LON;
      this.Lat=data.LAT;
      this.COURSE=data.COURSE;
      this.FLAG=data.FLAG;
      DB.child(data.SHIP_ID).update(shipInput);

    }

    })
  }

  click_hire(){

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    console.log(sessionStorage.getItem("firstname"))
    console.log(sessionStorage.getItem("lastname"))
    var that = this;
    if (this.userStatus) {

      var senderID = this.afAuth.auth.currentUser.uid;
      var DB =  this.afDB.database.ref().child("Hire_requests");
      var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO , created_at:this.currentTime,
      updated_at:this.currentTime}
      console.log(input);
      DB.push(input);
      this.mailSender.sendHireMail( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, that.id, this.ShipIMO);
      loading.dismiss();
      let alert =  this.alertCtrl.create({
        title: 'Email Sent',
        subTitle: 'We will contact you soon',
        buttons: ['Dismiss']
      });
      alert.present();

    }else{

      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port");
      loading.dismiss();
      that.navCtrl.push(Login);

    }
  }

click_sub_input(){

  let PaymentModal = this.modalCtrl.create(ShipInformationPayInfo, { ShipId: this.id } ,{showBackdrop: true, enableBackdropDismiss: true});
   PaymentModal.present();
  
  }

  click_sub(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    var that = this;
    if (this.userStatus) {
      var senderID =  this.afAuth.auth.currentUser.uid;
      var DB =  this.afDB.database.ref().child("put_sub");
      var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO, created_at:this.currentTime,
      updated_at:this.currentTime}
      DB.push(input);
      that.mailSender.sendSubMail( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, this.id, this.ShipIMO);
      loading.dismiss();
      let alert =  that.alertCtrl.create({
        title: 'Email Sent',
        subTitle: 'We will contact you soon',
        buttons: ['Dismiss']
      });
      alert.present();
    }else{
      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port")
      loading.dismiss();
      that.navCtrl.push(Login)

    }
  }
  click_Request_Inpection(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    var that = this;
    if (this.userStatus) {
      var senderID =  this.afAuth.auth.currentUser.uid;
      var DB =  this.afDB.database.ref().child("req_Inpection");
      var input = {senderEmail: that.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO, created_at:this.currentTime,
      updated_at:this.currentTime}
      DB.push(input);
      that.mailSender.sendRequestInpection( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, this.id, this.ShipIMO);
      loading.dismiss();
      let alert =  this.alertCtrl.create({
        title: 'Email Sent',
        subTitle: 'We will contact you soon',
        buttons: ['Dismiss']
      });
      alert.present();
    }else{
      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port")
      loading.dismiss();
      that.navCtrl.push(Login)

    }

  }
  click_Survey_Realtime(){

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    var that = this;
    if (this.userStatus) {
      var senderID =  this.afAuth.auth.currentUser.uid;
      var DB =  this.afDB.database.ref().child("sur_realtime");
      var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO, created_at:this.currentTime,
      updated_at:this.currentTime}
      DB.push(input);
      that.mailSender.sendSurveyRealtime( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, this.id, this.ShipIMO);
      loading.dismiss();
      let alert =  this.alertCtrl.create({
        title: 'Email Sent',
        subTitle: 'We will contact you soon',
        buttons: ['Dismiss']
      });
      alert.present();
    }else{
      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port")
      loading.dismiss();
      that.navCtrl.push(Login);
    }
  }

  click_View_gallery(){
    let profileModal = this.modalCtrl.create(Imageslide, { ShipId: this.id } ,{showBackdrop: true, enableBackdropDismiss: true});
    profileModal.present();
  }
  click_Set_Inpection(){

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    var that = this;
    if (this.userStatus) {
      var senderID =  this.afAuth.auth.currentUser.uid;
      var DB =  this.afDB.database.ref().child("set_inpection");
      var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO, created_at:this.currentTime,
      updated_at:this.currentTime}
      DB.push(input);
      this.mailSender.sendSetInpection( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, this.id, this.ShipIMO);
      loading.dismiss();
      let alert =  this.alertCtrl.create({
        title: 'Email Sent',
        subTitle: 'We will contact you soon',
        buttons: ['Dismiss']
      });
      alert.present();
    }else{

      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port");
      loading.dismiss();
      that.navCtrl.push(Login)

    }

  }
request_the_9881000(){
let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
    var that = this;

      if (this.userStatus) {
var senderID =  this.afAuth.auth.currentUser.uid;
  var DB =  this.afDB.database.ref().child("request_9881000");
var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO, created_at:this.currentTime,
updated_at:this.currentTime}
DB.push(input);
 that.mailSender.sendRequestThe9881000( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, this.id, this.ShipIMO);
loading.dismiss();
let alert =  this.alertCtrl.create({
    title: 'Email Sent',
    subTitle: 'We will contact you soon',
    buttons: ['Dismiss']
  });
  alert.present();
      }else{
        sessionStorage.setItem("Page",'ShipInformation');
        sessionStorage.setItem("ShipID",this.id);
        sessionStorage.setItem("Status","Port");
        loading.dismiss();
        that.navCtrl.push(Login)

      }
}
  other_document_input(){
    let alert = this.alertCtrl.create({
      title: 'Enter Document Name',
      inputs: [
        {
          name: 'Document',
          placeholder: 'Document Name'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enter',
          handler: data => {
            this.Request_other_document(data.Document)
          }
        }
      ]
    });
    alert.present();
  }

  Request_other_document(documentName){

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    var that = this;
    if (this.userStatus) {
    var senderID =  this.afAuth.auth.currentUser.uid;
    var DB =  this.afDB.database.ref().child("req_other_document");
    var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO,
                 documentname: documentName,created_at:this.currentTime,updated_at:this.currentTime}
    DB.push(input);
    that.mailSender.sendRequestOtherDocument( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, this.id, this.ShipIMO, documentName);
    loading.dismiss();
    let alert =  this.alertCtrl.create({
      title: 'Email Sent',
      subTitle: 'We will contact you soon',
      buttons: ['Dismiss']
    });
    alert.present();
    }else{
      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port");
      loading.dismiss();
      that.navCtrl.push(Login);

    }

  }
  add_to_my_fleet(){
  let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  var that=this;
  if(this.userStatus){
    var userID =  this.afAuth.auth.currentUser.uid;
    var input = {id: this.id, name : this.ShipName, created_at:this.currentTime,
    updated_at:this.currentTime}
    var DB=this.afDB.database.ref().child("userFleet");
    DB.child(userID).push().set(input)
    let alert =  this.alertCtrl.create({
      title: 'Action Successful',
      subTitle: 'Ship Added to Fleet',
      buttons: ['Dismiss']
    });
    alert.present();
  }else{
    sessionStorage.setItem("Page",'ShipInformation');
    sessionStorage.setItem("ShipID",this.id);
    sessionStorage.setItem("Status","Port");
    loading.dismiss();
    that.navCtrl.push(Login);
  }
  loading.dismiss();
  }
  request_the_q88(){

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    var that = this;
    if (this.userStatus) {
      var senderID =  this.afAuth.auth.currentUser.uid;
      var DB =  this.afDB.database.ref().child("request_q88");
      var input = {senderEmail: this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),shipname: this.ShipName,shipid: this.id,ShipIMO: this.ShipIMO, created_at:this.currentTime,
      updated_at:this.currentTime}
      DB.push(input);
      that.mailSender.sendRequestTheq88( this.senderEmail,sessionStorage.getItem('phoneno'),senderID, this.ShipName, that.id, this.ShipIMO);
      loading.dismiss();
      let alert =  this.alertCtrl.create({
        title: 'Email Sent',
        subTitle: 'We will contact you soon',
        buttons: ['Dismiss']
      });
      alert.present();
    }else{

      sessionStorage.setItem("Page",'ShipInformation');
      sessionStorage.setItem("ShipID",this.id);
      sessionStorage.setItem("Status","Port");
      loading.dismiss();
      that.navCtrl.push(Login)

    }
  }


}
