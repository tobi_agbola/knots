import { Component , OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController , MenuController , ModalController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {} from '@types/googlemaps';
import { Observable } from 'rxjs/Observable';
import { SimulateShips } from '../../providers/simulate-ships';
import { UserStatus  } from '../../providers/user-status';
import { MapGeocoder } from '../../providers/map-geocoder';
import { ShippingOrders } from '../../pages/shipping-orders/shipping-orders';
import {ShipInformation} from '../../pages/ship-information/ship-information';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Packages } from '../../pages/packages/packages';
import * as firebase from 'firebase/app';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { ShipPreview } from '../../components/ship-preview/ship-preview';
import { WorkThroughModal } from '../../components/work-through-modal/work-through-modal';
import { ShipSearchComponent } from '../../components/ship-search-component/ship-search-component';

/**
 * Generated class for the Home page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google : any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class Home implements OnInit {
public map: google.maps.Map;
public marker;
public Search;
public currentTime= new Date().getTime();
public topSearch=false;
public userLoggedIn = false;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public ShipSimulation: SimulateShips, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase ,public mapGeocoder: MapGeocoder, public loadingCtrl: LoadingController,private alertCtrl: AlertController, public menuCtrl: MenuController,public user: UserStatus,private ga: GoogleAnalytics, public geolocation: Geolocation, public modalCtrl: ModalController) {

    this.GoogleTracker();


}

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
  }
  public navBarstatus = true;
  ngOnInit(){

    this.workThroughModalPop();
    this.statusObserver();
    this.map = this.createMap();
    this.getCurrentLocation().subscribe(location =>{
      this.centerLocation(location);
      this.placeIcons();
    })

  }

  statusObserver(){

    var that = this;
    firebase.auth().onAuthStateChanged(function(user) {

      if (user) {

        that.userLoggedIn = true;
        that.menuCtrl.enable(false, 'unauthenticated');
        that.menuCtrl.enable(true,'authenticated')
        that.GetUserInfo();

        //that.user.GetUserInfo();

      if(sessionStorage.getItem("status")=="waiting"){

        that.getPackages();

      }
      //that.menuCtrl.enable(false, 'unauthenticated');
      } else {

        that.userLoggedIn = false;
        that.menuCtrl.enable(false, 'authenticated');
        that.menuCtrl.enable(true, 'unauthenticated');

      }
    });

  }
  workThroughModalPop(){

    if (sessionStorage.getItem("modalStatus")) {

      sessionStorage.setItem("modalStatus","Done");

    } else {
      sessionStorage.setItem("modalStatus","Done");
      let modal = this.modalCtrl.create(WorkThroughModal);
      modal.present();
    }


  }
  shipPreviewModalPop(Ship_name,ShipId,Flag,IMO,ShipType){
    console.log(Ship_name);
    let modal = this.modalCtrl.create(ShipPreview, { Shipname: Ship_name,ShipID: ShipId, Flag:Flag ,IMO: IMO,ShipType:ShipType  });
      modal.present();
  }
  shipSearchModalPop(){
    let modal = this.modalCtrl.create(ShipSearchComponent);
      modal.present();
  }

  GetUserInfo(){
    //Gets and stores users infomation in sessionStorage
    var DB=this.afDB.database.ref().child("Users");
    var input = {last_login:this.currentTime}
    DB.child(this.afAuth.auth.currentUser.uid).update(input)
    DB.child(this.afAuth.auth.currentUser.uid).once('value').then(data=>{
      console.log(data.val())
      if (typeof(Storage) !== "undefined") {

          if(sessionStorage.getItem("UID")==undefined){

            sessionStorage.setItem("UID",this.afAuth.auth.currentUser.uid);
            sessionStorage.setItem("country",data.val().country);
            sessionStorage.setItem("company",data.val().company);
            sessionStorage.setItem("firstname",data.val().firstname);
            sessionStorage.setItem("lastname",data.val().lastname);
            sessionStorage.setItem("status", data.val().status);
            sessionStorage.setItem("Start_Time", data.val().Start_Time);
            sessionStorage.setItem("Stop_Time", data.val().Stop_Time);
            sessionStorage.setItem("phoneno",data.val().phoneno);
            console.log(sessionStorage.getItem("phoneno"));


          }
      if(sessionStorage.getItem("status")=="waiting"){

        this.getPackages();

      }
      if(this.currentTime>=parseInt(sessionStorage.getItem("Stop_Time"))){

        this.getPackages();

      }

    }
   else {

      console.log("Error No Webstorage Support")
      if(sessionStorage.getItem("status")=="waiting"){
        this.getPackages();
      }

    }

    })
  }
  getPackages(){
    //Moves view to packages page
    this.navCtrl.setRoot(Packages);

  }
  createMap(location= new google.maps.LatLng(37.0902, 95.7129)){
  //Create Map
  let mapOptions = {
    center: location,
    zoom: 4,
    mapTypeId: google.maps.MapTypeId.TERRAIN,
    disableDefaultUI:true
  }

  let mapEL = document.getElementById('map');

  return new google.maps.Map(mapEL, mapOptions)

  }

  getCurrentLocation(){
    //get Current Location
    //console.log("function")
    let locationObs=Observable.create(observer =>{
    let options = {timeout: 10000, enableHighAccuracy: true};
    this.geolocation.getCurrentPosition(options).then(resp => {

        let lat = resp.coords.latitude;
        let lng = resp.coords.longitude;
        let location = new google.maps.LatLng(lat, lng);
        //console.log('Geolocation: ' + location);
        observer.next(location);

      },
      (err) => {
        //console.log('Geolocation err: ' + err);
      })
  })
  return locationObs;

  }

  centerLocation(location) {
  //move map to current location
    if (location) {
      // this.currentlocation=location;
      this.map.panTo(location);

    }
    else {

      this.getCurrentLocation().subscribe(currentLocation => {
        //this.currentlocation=currentLocation;
        //Moves map to users current location
        this.map.panTo(currentLocation);
      });
    }
  }

  placeIcons(){
  //this Function Gets the markers from the ShipSimulation Service and initializes the map with those data points
      this.ShipSimulation.getShips().subscribe(data =>{
        var myLatLng = {lat: data.LAT, lng: data.LON}
        var Ship_Id=data.SHIP_ID.toString();
        var Ship_name=data.SHIPNAME.toString();
        var that = this;
        var lastport=data.LAST_PORT.toString();
        var Flag = data.FLAG.toString();
        var IMO = data.IMO.toString();
        var ShipType = data.TYPE_NAME.toString();

      this.mapGeocoder.getLatLng(data.DESTINATION).subscribe(desData=>{
        this.mapGeocoder.getLatLng(lastport).subscribe(desData=>{
        })
      })
      var image = this.getIconImage(data.AIS_TYPE_SUMMARY)
      console.log(myLatLng)
      this.marker = new google.maps.Marker({
        position: myLatLng,
        title: Ship_name,
        label:{text:Ship_name,
        fontSize:"9px",
        position:"bottom"
    },
    map: this.map,
    icon: {
          path: image['path'],
          fillColor:image['color'],
          fillOpacity: 1,
          strokeColor: image['color'],
          strokeWeight: 1,
          scale: 3,
          rotation: data.HEADING
          },
    clickable: true,
    });

    this.marker.ShipId=Ship_Id;
    //var mark= document.getElementById(Ship_Id);
    //console.log(mark)
    //document.getElementById(Ship_Id).style.transform='rotate(90 deg)';
    this.marker.addListener('click', function() {
    this.getTitle();
    //that.GetShipInformation(this.ShipId);
    that.shipPreviewModalPop(Ship_name,this.ShipId,Flag,IMO,ShipType);
        });
    })
  }

  getShippath(destination,currentlocation,lastport){
    var shipPath = new google.maps.Polyline({
      path:[
      destination,
      currentlocation,
      lastport],
      geodesic: true,
      strokeColor: '#0ABF00',
      strokeOpacity: 1.0,
      strokeWeight: 1
    });
    shipPath.setMap(this.map);
    }
  getlongLat(address : string){
    var result={};
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address':address},function(results,status){

    if(status===google.maps.GeocoderStatus.OK){
      result['lat']=results[0].geometry.location.lat;
      result['lng']=results[0].geometry.location.lng.toString();
    }else {
      alert('test else')
      return'Geocode was not successful for the following reason: ' + status;
     }
  });

  return result;

  }

  getIconImage(shipType : string){
    // this Function converts the vessel type to to the appropriate Color and vessel Icon
    var options={};
    if(shipType == "Fishing"){
      options['color']='#E95644';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }else if(shipType == "Tug"){
      options['color']='#2CB96D';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }else if(shipType == "Navigation Aids"){
      options['color']='#2B7DE1';
      options['path']=google.maps.SymbolPath.CIRCLE;
      return options;
    }else if(shipType == "Pleasure Craft"){
      options['color']='#E98925';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }else if(shipType == "Passenger"){
      options['color']='#A564BE';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }else if(shipType == "Cargo"){
      options['color']='#D3462A';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }else if(shipType == "Tanker"){
      options['color']='#050201';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }else{
      options['color']='#E03434';
      options['path']=google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
      return options;
    }
  }

  GetShipInformation(Ship_Id){
    console.log(this.userLoggedIn);
    // if(sessionStorage.getItem("UID")){
      this.navCtrl.push(ShipInformation,{
        "Ship_Id" : Ship_Id
      });
    // }else{
    //   let alert = this.alertCtrl.create({
    //     title: 'Login to access this feature',
    //     buttons: ['Dismiss']
    //   });
    //   alert.present();
    // }
  }


  navBarToggle(){
    //this Function Toggles the header of the map and sets the zoom level too.
    if(this.navBarstatus==true){
      //zooms in and removes the header by setting navBarStatus to false
      this.map.setZoom(7);
      this.navBarstatus=false;

    }else{
      //zooms out and add the header by setting navBarStatus to true
      this.map.setZoom(4);
      this.navBarstatus=true;

    }
  }

  GoogleTracker(){

    this.ga.startTrackerWithId('UA-113802115-1')
    .then(() => {

      console.log('Google analytics is ready now');

        this.ga.trackView('test');

        if(this.afAuth.auth.currentUser.uid.toString()){

          this.ga.setUserId(this.afAuth.auth.currentUser.uid.toString());

        }else{

          this.ga.setUserId('guest');

        }

      // Tracker is ready
      // You can now track pages or set additional information such as AppVersion or UserId
  })
  .catch(e => console.log('Error starting GoogleAnalytics', e));

  }
}
