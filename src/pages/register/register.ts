import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController  } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Home } from '../../pages/home/home';
/**
 * Generated class for the Register page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {
  test="test";
public FormData={};
public currentTime= new Date().getTime();
public phone;
public formstatus;
public errorMessage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase, public loadingCtrl: LoadingController,private alertCtrl: AlertController) {
this.statusObserver();
 }
 statusObserver(){
    var that = this;
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
  that.navCtrl.setRoot(Home)
  } else {

  }
});

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Register');
  }
  FormValidator(){
    this.formstatus=true;
   if(!this.FormData['firstname']){ this.formstatus=false; }
   if(!this.FormData['password']){ this.formstatus=false; }
   if(this.FormData['password']!=this.FormData['retypepassword']){ this.formstatus=false;  }
   if(!this.FormData['lastname']){ this.formstatus=false; }
   if(!this.FormData['country']){ this.formstatus=false; }
   if(!this.FormData['company']){ this.formstatus=false; }
   if(!this.FormData['phoneno']){ this.formstatus=false; }
   if(this.formstatus){
     this.RegisterForm()
   }else{
     this.errorMessage="All fields Are Required";
   }
  }
  RegisterForm(){
    let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
console.log(this.FormData)
//form validation
loading.present();

this.afAuth.auth.createUserWithEmailAndPassword(this.FormData['email'],this.FormData['password']).then(data =>{
var DB=this.afDB.database.ref().child("Users");
var userInput ={firstname:this.FormData['firstname'], lastname:this.FormData['lastname'],country:this.FormData['country'],
                 company:this.FormData['company'],
                 phoneno:this.FormData['phoneno'],
                 status:'waiting',
                 created_at:this.currentTime,
                 last_login:this.currentTime
          }
      DB.child(data.uid).set(userInput);
      loading.dismiss();
      this.navCtrl.pop();

}).catch(err=>{
  console.log(err)
let alert = this.alertCtrl.create({
    title: 'Error Message',
    subTitle: err.message,
    buttons: ['Dismiss']
  });
  alert.present();
  loading.dismiss();
})



  }
  popRegisterForm(){
    this.navCtrl.pop();
  }

}
