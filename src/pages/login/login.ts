import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { Register } from '../../pages/register/register';
import { Home } from '../../pages/home/home';
import { ForgotPassword} from '../../pages/forgot-password/forgot-password';
import {AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import {ShipInformation} from '../../pages/ship-information/ship-information';
import { UserStatus } from '../../providers/user-status';
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
public FormData={};
public errorMessage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public afAuthModule :AngularFireAuthModule , public afAuth: AngularFireAuth, public userStatus: UserStatus, public loadingCtrl: LoadingController, public menuCtrl: MenuController) {
    //console.log(this.userStatus.CheckLogin())
    //this.AutoLogin();

    this.closemenu();

 }

  ionViewDidLoad() {

    console.log('ionViewDidLoad Login');

  }
  getRegister() {

    //Moves View to register page

    this.navCtrl.push(Register);

  }
  AutoLogin() {
    if(typeof sessionStorage.getItem("customtoken")!=="undefined" && sessionStorage.getItem("customtoken")!== "undefined"){

    this.afAuth.auth.signInWithCustomToken(sessionStorage.getItem("customtoken")).then(data=>{

      if (typeof(Storage) !== "undefined") {

        sessionStorage.setItem("customtoken",data.refreshToken);

      }

    this.getHome()}).catch(err =>{ console.log(err)});

  }

  }
  getHome() {
    //Moves view to Home page

    this.navCtrl.setRoot(Home);

  }
  getShipInformation(Ship_Id) {

    this.navCtrl.pop();

    this.navCtrl.push(ShipInformation,{"Ship_Id" : Ship_Id});

  }

  getForgotPassword() {

    this.navCtrl.push(ForgotPassword);

  }
  closemenu() {

    this.menuCtrl.close();

  }
  signIn()  {
    let loading = this.loadingCtrl.create({content: 'Please wait...'});

    loading.present();

    var that = this;

    if(this.FormData['email'] && this.FormData['password']){

      this.afAuth.auth.signInWithEmailAndPassword(this.FormData['email'],this.FormData['password']).then(data=>{

        loading.dismiss();

      if(sessionStorage.getItem('Page')=='ShipInformation'){

        sessionStorage.clear();

        sessionStorage.setItem('splash', 'true'); //set splashScreen

        that.getHome();

      }else{
        if(sessionStorage.getItem('UID')){

          sessionStorage.clear();

        }
        sessionStorage.setItem('splash', 'true'); //set splashScreen

        that.getHome();

      }
      }).catch(err =>{
          loading.dismiss();

          this.errorMessage = err.message;

        });
    }else{
      this.errorMessage="Empty Email or Password";
    }

    loading.dismiss();

  }

}
