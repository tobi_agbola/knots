import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShipsNearMe } from './ships-near-me';

@NgModule({
  declarations: [
    ShipsNearMe,
  ],
  imports: [
    IonicPageModule.forChild(ShipsNearMe),
  ],
  exports: [
    ShipsNearMe
  ]
})
export class ShipsNearMeModule {}
