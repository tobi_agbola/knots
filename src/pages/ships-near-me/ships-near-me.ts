import { Component, OnInit  } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {} from '@types/googlemaps';
import {Observable} from 'rxjs/Observable';
import { SimulateShips } from '../../providers/simulate-ships';
import {ShipInformation} from '../../pages/ship-information/ship-information';
/**
 * Generated class for the ShipsNearMe page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-ships-near-me',
  templateUrl: 'ships-near-me.html',
})
export class ShipsNearMe implements OnInit  {

  constructor(public navCtrl: NavController, public navParams: NavParams,public ShipSimulation: SimulateShips, public loadingCtrl: LoadingController, public geolocation: Geolocation) {
  }
public shipsNearMe=[];
public emptyStatus=false;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShipsNearMe');
  }
  ngOnInit(){
this.getShips();
  }
  getCurrentLocation(){
let locationObs=Observable.create(observer =>{
      let options = {timeout: 10000, enableHighAccuracy: true};
     this.geolocation.getCurrentPosition(options)
     .then(resp => {
          let lat = resp.coords.latitude;
          let lng = resp.coords.longitude;

          let location = new google.maps.LatLng(lat, lng);

          console.log('Geolocation: ' + location);

          observer.next(location);


        },
        (err) => {
          console.log('Geolocation err: ' + err);
        })
    })
return locationObs;
  }
getShips(){
let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

var input;
this.getCurrentLocation().subscribe(latlng =>{
 this.ShipSimulation.getShips().subscribe(data => {
       if(data.LAT>=(latlng.lat()-1) || data.LAT<=(latlng.lat()+1)){
  input={id:data.SHIP_ID , name: data.SHIPNAME }
this.shipsNearMe.push(input);
      }else if(data.LON<=(latlng.lng()+1) || data.LON>=(latlng.lng()-1)){
        input={id:data.SHIP_ID , name: data.SHIPNAME }
         this.shipsNearMe.push(input);
      }else{
console.log("data long"+data.LON);
console.log("data lat"+data.LAT);
console.log("latlng lat"+latlng.lat());
console.log("latlng lon"+latlng.lng());
console.log("test messi");
      }
    }, error =>{
      console.log(error)
      loading.dismiss();
    })
    if(input){

    }
})
    loading.dismiss();
}
ShipInformation(Ship_Id){
    this.navCtrl.push(ShipInformation,{
      "Ship_Id" : Ship_Id
    })
}

}
