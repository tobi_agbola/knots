import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController  } from 'ionic-angular';
import { Mailsender } from '../../providers/mailsender';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Login } from '../../pages/login/login';
/**
 * Generated class for the Feedback page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class Feedback {

  constructor(public navCtrl: NavController, public navParams: NavParams, public mailSender: Mailsender, public loadingCtrl: LoadingController,private alertCtrl: AlertController, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase) {
  }
public FormData = {};
public currentTime= new Date().getTime();
public senderEmail=this.afAuth.auth.currentUser.email;
public senderID = this.afAuth.auth.currentUser.uid;
  ionViewDidLoad() {
    console.log('ionViewDidLoad Feedback');
  }
click_feedback(){
     let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  var DB = this.afDB.database.ref().child("feedback");
var input = {senderEmail:this.senderEmail,phoneno:sessionStorage.getItem('phoneno'),subject:this.FormData['subject'],body:this.FormData['body'], created_at:this.currentTime,
updated_at:this.currentTime}
DB.push(input);
this.mailSender.sendFeedbackMail(this.senderEmail,sessionStorage.getItem('phoneno'),this.senderID,this.FormData['subject'],this.FormData['body']);
loading.dismiss();
let alert = this.alertCtrl.create({
    title: 'Email Sent',
    subTitle: 'Nice to hear from you',
    buttons: ['Dismiss']
  });
  alert.present();
this.FormData['subject']="";
this.FormData['body']="";
}
  statusObserver(){
    var that = this;
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {

  } else {
   that.navCtrl.setRoot(Login)
  }
});

  }

  popToHome(){
    this.navCtrl.pop();
  }
}

