import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Packages } from './packages';

@NgModule({
  declarations: [
    Packages,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  imports: [
    IonicPageModule.forChild(Packages),
  ],
  exports: [
    Packages
  ]
})
export class PackagesModule {}
