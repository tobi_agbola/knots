import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, LoadingController  } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { PaystackApiCalls } from '../../providers/paystack-api-calls';
import { Home } from '../../pages/home/home';
import { AngularFireDatabase } from 'angularfire2/database';
import { Angular4PaystackModule } from 'angular4-paystack';
import { CardChoiceComponent } from '../../components/card-choice-component/card-choice-component';

/**
 * Generated class for the Packages page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-packages',
  templateUrl: 'packages.html',
})
export class Packages {

  constructor(public navCtrl: NavController,private alertCtrl: AlertController, public navParams: NavParams, public PayStack: Angular4PaystackModule ,public afAuth: AngularFireAuth, public afDB: AngularFireDatabase, public PayStackAPI: PaystackApiCalls, public modalCtrl: ModalController, public loadingCtrl: LoadingController) {
    this.setButton();
  }

  public userEmail = this.afAuth.auth.currentUser.email.toString();
  public refNo=Math.floor(100000000 + Math.random() * 900000000) + sessionStorage.getItem("UID");
  public currentTime= new Date().getTime();
  public nowDate = new Date();
  public year = this.nowDate.getFullYear();
  public month = this.nowDate.getMonth();
  public day = this.nowDate.getDate();
  public datePlusYear = new Date(this.year + 1, this.month, this.day).getTime();
  public datePlusMonth = new Date(this.year + 1, this.month, this.day).getTime();
  public DB=this.afDB.database.ref();
  public btnFreePackage=true;
  public btnBasicMonthly=true;
  public btnBasicYearly=true;
  public btnPremiumMonthly=true;
  public btnPremiumYearly=true;
  ionViewDidLoad() {
    console.log('ionViewDidLoad Packages');
  }
  setButton(){
    if(sessionStorage.getItem("status")){
      if(sessionStorage.getItem("status")=="Free"){

        this.btnFreePackage=false;

      }else if(sessionStorage.getItem("status")=="Basic Yearly"){

        this.btnBasicYearly=false;

      }else if(sessionStorage.getItem("status")=="Basic Monthly"){

        this.btnBasicMonthly=false;
        
      }else if(sessionStorage.getItem("status")=="Premium Yearly"){

        this.btnPremiumYearly=false;
        
      }else if(sessionStorage.getItem("status")=="Premium Monthly"){
        
        this.btnPremiumMonthly=false;

      }
    }
  }

  SetFree() {//Moves view to Home page
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    if(sessionStorage.getItem("status")=="waiting") {
      var Input ={status:"Free",Start_Time:this.currentTime,Stop_Time:this.datePlusMonth, Used_Free:"yes" };

      var InputLog ={UID : sessionStorage.getItem("UID"),Cost:0, Subscription:"Free",Start_Time:this.currentTime,Stop_Time:this.datePlusMonth};

      this.DB.child("Users").child(sessionStorage.getItem("UID")).update(Input);

      this.DB.child("Subscription_log").push().set(InputLog);

      sessionStorage.setItem("status","Free");

      this.navCtrl.setRoot(Home);

      loading.dismiss();

    } else {

      loading.dismiss();

      let alert = this.alertCtrl.create({
        title: 'Subscription Rejected',
        subTitle: 'Free Trial is only avaliable to first time users',
        buttons: ['Dismiss']
      });

      alert.present();
    }

  }

  setRootHome() {

  this.navCtrl.setRoot(Home);

  }


  paymentCancel(){

  console.log("test");

  }
  getPaymentChoiceModal(userChoice) {

    const paymentChoiceModalPop = this.modalCtrl.create( CardChoiceComponent , { packageType : userChoice.packageType , packageDuration : userChoice.packageDuration , packageCost : userChoice.packageCost});

    paymentChoiceModalPop.present();

    paymentChoiceModalPop.onDidDismiss(() => {

      if(sessionStorage.getItem("status")=="Basic Yearly"){

        this.setRootHome();

      }else if(sessionStorage.getItem("status")=="Basic Monthly") {

        this.setRootHome();

      }else if(sessionStorage.getItem("status")=="Premium Yearly") {

        this.setRootHome();

      }else if(sessionStorage.getItem("status")=="Premium Monthly") {

        this.setRootHome();

      }

    })

  }

}
