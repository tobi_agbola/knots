import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { Home } from '../../pages/home/home';
import * as firebase from 'firebase/app';
/**
 * Generated class for the ForgotPassword page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPassword {

  constructor(public navCtrl: NavController, public navParams: NavParams ,public afAuth: AngularFireAuth) {
    this.statusObserver();
  }
public email;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPassword');
  }
  statusObserver(){

  var that = this;

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {

    that.navCtrl.setRoot(Home);

    } else {

    }
  });

  }

ForgotPasswordForm(){
  if(this.email != null){

   this.afAuth.auth.sendPasswordResetEmail(this.email);
    this.navCtrl.pop();

  }

}
popRegisterForm(){
  this.navCtrl.pop();
}
}
