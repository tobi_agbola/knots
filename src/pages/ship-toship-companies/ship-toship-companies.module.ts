import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShipToshipCompanies } from './ship-toship-companies';

@NgModule({
  declarations: [
    ShipToshipCompanies,
  ],
  imports: [
    IonicPageModule.forChild(ShipToshipCompanies),
  ],
  exports: [
    ShipToshipCompanies
  ]
})
export class ShipToshipCompaniesModule {}
