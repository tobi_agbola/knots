import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { CompaniesContactInfo } from '../../components/companies-contact-info/companies-contact-info';
/**
 * Generated class for the ShipToshipCompanies page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-ship-toship-companies',
  templateUrl: 'ship-toship-companies.html',
})
export class ShipToshipCompanies {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShipToshipCompanies');
  }

  viewSTSContactDetails(){
    const CompaniesContactInfoModal = this.modalCtrl.create(CompaniesContactInfo, { companyID : 8675309 , companyType : 8675309 , companyName : "Glenstarmarine" , companyEmail : "info@glenstarmarine.com" , companyNumber : 8019042803 , companyAddress : "Mobolaji Johnson Estate Lekki Lagos" , companyLogo : "http://www.glenstarmarine.com/images/logo.png", companyDescription:"STS dskjsdnksf;vwenroepomve"  });
    CompaniesContactInfoModal.present();
  }

}
