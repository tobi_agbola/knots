import { Component } from '@angular/core';

/**
 * Generated class for the AddOtherDocument component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'add-other-document',
  templateUrl: 'add-other-document.html'
})
export class AddOtherDocument {

  text: string;

  constructor() {
    console.log('Hello AddOtherDocument Component');
    this.text = 'Hello World';
  }

}
