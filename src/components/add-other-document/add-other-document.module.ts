import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { AddOtherDocument } from './add-other-document';

@NgModule({
  declarations: [
    AddOtherDocument,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    AddOtherDocument
  ]
})
export class AddOtherDocumentModule {}
