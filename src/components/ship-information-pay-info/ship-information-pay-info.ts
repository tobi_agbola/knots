import { Component , ViewChild } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { FirebaseDatabaseCalls } from '../../providers/firebase-database-calls';
import { Angular4PaystackModule } from 'angular4-paystack';

/**
 * Generated class for the ShipInformationPayInfo component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'ship-information-pay-info',
  templateUrl: 'ship-information-pay-info.html'
})
export class ShipInformationPayInfo {

  text: string;
  public Cards;
  public CardsKeys;
  constructor(public viewCtrl: ViewController,public params: NavParams, public firebaseDatabase : FirebaseDatabaseCalls, public PayStack: Angular4PaystackModule) {
    console.log('Hello ShipInformationPayInfo Component');
    this.text = 'Hello World';
  }
  dismissmodal() {
    this.viewCtrl.dismiss();
  }
  LoadCards(){
    var that = this;
    this.firebaseDatabase.AccountFirebaseInit().then(snapshot => {
      that.CardsKeys = Object.keys(snapshot.val());
      that.Cards = snapshot.val();
      console.log(snapshot.val());
    }).catch(err =>{
      console.log(err);
    });
  }
}
