import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ShipInformationPayInfo } from './ship-information-pay-info';

@NgModule({
  declarations: [
    ShipInformationPayInfo,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ShipInformationPayInfo
  ]
})
export class ShipInformationPayInfoModule {}
