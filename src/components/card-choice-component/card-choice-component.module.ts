import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CardChoiceComponent } from './card-choice-component';

@NgModule({
  declarations: [
    CardChoiceComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    CardChoiceComponent
  ]
})
export class CardChoiceComponentModule {}
