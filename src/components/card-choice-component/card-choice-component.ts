import { Component } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Angular4PaystackModule } from 'angular4-paystack';
import { NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { FirebaseDatabaseCalls } from '../../providers/firebase-database-calls';
import { SubscribeUsers } from '../../providers/subscribe-users';
import { Home } from '../../pages/home/home';
import { PaystackApiCalls } from '../../providers/paystack-api-calls';

/**
 * Generated class for the CardChoiceComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'card-choice-component',
  templateUrl: 'card-choice-component.html'
})
export class CardChoiceComponent {

  public userEmail = this.afAuth.auth.currentUser.email.toString();
  public refNo=Math.floor(100000000 + Math.random() * 900000000) + sessionStorage.getItem("UID");
  public subscriptionCost = this.params.get("packageCost");
  public subscriptionType = this.params.get("packageType");
  public subscriptionDuration = this.params.get("packageDuration");
  public loader;
  public Cards;
  public CardsKeys;
  public message;
  constructor(public navCtrl: NavController, public PayStack: Angular4PaystackModule, public afDB: AngularFireDatabase, public afAuth: AngularFireAuth, public viewCtrl: ViewController, public params: NavParams, public loadingCtrl: LoadingController, public subscribeUsers : SubscribeUsers, private alertCtrl: AlertController, public firebaseDatabase : FirebaseDatabaseCalls, public PayStackAPI: PaystackApiCalls ) {
    console.log('Hello CardChoiceComponent Component');
    this.LoadCards();
  }

  dismissmodal() {

    this.viewCtrl.dismiss();

  }

  paymentCancel() {

    this.stopLoader();

  }

  startLoader(){

    this.loader = this.loadingCtrl.create({

      content: 'Please wait...'

    });

    this.loader.present();

  }

  stopLoader(){

    this.loader.dismiss();

  }

  paySubscription($event) {

    this.stopLoader();

    console.log(this.subscriptionType +" "+this.subscriptionDuration);

    if(this.subscriptionType == "Basic" && this.subscriptionDuration == "Monthly"){

      this.subscribeUsers.subscribeBasicMonthly($event);

      let alert = this.alertCtrl.create({
        title: 'Subscription Successful',
        subTitle: 'Successful',
        buttons: ['Dismiss']
      });
      alert.present();

    }else if(this.subscriptionType == "Basic" && this.subscriptionDuration == "Yearly"){

      this.subscribeUsers.subscribeBasicYearly($event);

      let alert = this.alertCtrl.create({
        title: 'Subscription Successful',
        subTitle: 'Successful',
        buttons: ['Dismiss']
      });
      alert.present();


    }else if(this.subscriptionType == "Premium" && this.subscriptionDuration == "Monthly"){

      this.subscribeUsers.subscribePremiumMonthly($event);

      let alert = this.alertCtrl.create({
        title: 'Subscription Successful',
        subTitle: 'Successful',
        buttons: ['Dismiss']
      });
      alert.present();


    }else if(this.subscriptionType == "Premium" && this.subscriptionDuration == "Yearly"){

      this.subscribeUsers.subscribePremiumYearly($event);

      let alert = this.alertCtrl.create({
        title: 'Subscription Successful',
        subTitle: 'Successful',
        buttons: ['Dismiss']
      });
      alert.present();


    }else{

      let alert = this.alertCtrl.create({
        title: 'InValid Subscription Package',
        subTitle: 'Error',
        buttons: ['Dismiss']
      });
      alert.present();
    }
     this.dismissmodal();

  }
    LoadCards(){
    var that = this;
    this.firebaseDatabase.AccountFirebaseInit().then(snapshot => {
      that.CardsKeys = Object.keys(snapshot.val());
      that.Cards = snapshot.val();
      console.log(snapshot.val());
    }).catch(err =>{
      that.message="empty";
      console.log(err);
    });
  }
  payWithStoredCard(CardRequestObject){
    var that = this;
    let alert = this.alertCtrl.create({
      title: 'Confirm purchase of subscription',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("BuyClicked");
          }
        },
        {
          text: 'Pay',
          handler: () => {
            that.billCard(CardRequestObject);
          }
        }
      ]
    });
    alert.present();
  }
  billCard(CardRequestObject){
    var that = this;
    this.startLoader();
    this.PayStackAPI.recurrentBilling(CardRequestObject.authorizationCode,CardRequestObject.email,CardRequestObject.subscriptionCost).subscribe(
      (data) =>{
        if(data.status == true){
          that.paySubscription(data.data);
        }else{
          let alert = that.alertCtrl.create({
            title:"Failed Transaction",
            subTitle: data.message,
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                that.stopLoader();
              }
            }]
          });
          alert.present();
        }
        console.log(data);

      }
    );

  }
}
