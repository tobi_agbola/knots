import { Component, ViewChild} from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';

/**
 * Generated class for the Imageslide component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'imageslide',
  templateUrl: 'imageslide.html'
})
export class Imageslide {
  constructor(public viewCtrl: ViewController,public params: NavParams, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase) {

    this.getPhotos();
  }
  public gallery=[];

  @ViewChild(Slides) slides: Slides;

  dismissmodal() {

   this.viewCtrl.dismiss();

  }
  getPhotos(){

    var that= this;

    this.afDB.database.ref().child('shipGallery').child(this.params.get('ShipId')).once('value').then(function(snapshot) {

      if(snapshot.val()!= undefined) {

          var count = 0;

        for(let keys of snapshot.val()) {

        that.gallery[count]=snapshot.val().keys.URL;

        count++;

        }
      }else{

        that.gallery[0]="img/vessel_image/empty-gallery.fw.png";

      }
    }).catch(err => {console.log(err.message);
        this.gallery[0]="img/vessel_image/empty-gallery.fw.png";
      })
  }
}
