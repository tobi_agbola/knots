import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Imageslide } from './imageslide';

@NgModule({
  declarations: [
    Imageslide,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    Imageslide
  ]
})
export class ImageslideModule {}
