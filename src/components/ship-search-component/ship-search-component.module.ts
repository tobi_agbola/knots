import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ShipSearchComponent } from './ship-search-component';

@NgModule({
  declarations: [
    ShipSearchComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ShipSearchComponent
  ]
})
export class ShipSearchComponentModule {}
