import { Component } from '@angular/core';
import { NavController, NavParams, ViewController , LoadingController, AlertController } from 'ionic-angular';
import { SimulateShips } from '../../providers/simulate-ships';
import { ShipInformation } from '../../pages/ship-information/ship-information';

/**
 * Generated class for the ShipSearchComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'ship-search-component',
  templateUrl: 'ship-search-component.html'
})
export class ShipSearchComponent {

  public Search;
  public SearchResult;
  public emptyStatus = false;

  constructor(public navCtrl: NavController,public viewCtrl: ViewController,public params: NavParams , public loadingCtrl: LoadingController , private alertCtrl: AlertController , public ShipSimulation: SimulateShips ) {
    console.log('Hello ShipSearchComponent Component');
  }

  dismissmodal() {

    this.viewCtrl.dismiss();

  }

  SearchShips(){
    console.log(this.Search);
    var result="none";
    let loading = this.loadingCtrl.create({
    content: 'Please wait...'
    });
    if(this.Search){
      this.SearchResult = [];

      this.ShipSimulation.getShips().subscribe(data => {
      if(data.SHIPNAME.toUpperCase().includes(this.Search.toUpperCase())){
        loading.dismiss();
        //this.GetShipInformation(data.SHIP_ID)
        this.SearchResult.push(data);
        this.emptyStatus = false;
        result="Found";
      }

    }, error =>{

      console.log(error)
      loading.dismiss();

    })

    }else{

      // this.topSearch=false;
      result="empty";

    }

    loading.dismiss();
    if(result==="none"){
        this.emptyStatus = true;
        console.log("none");
        // let alert = this.alertCtrl.create({
        //   title: this.Search+' not Found',
        //   subTitle: 'Please correct any spelling errors',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
    }
    console.log(this.SearchResult);
  }
  GetShipInformation(Ship_Id){
    if(sessionStorage.getItem("UID")){
      this.navCtrl.push(ShipInformation,{
        "Ship_Id" : Ship_Id
      })
    }else{
      let alert = this.alertCtrl.create({
        title: 'Login to access this feature',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }

}
