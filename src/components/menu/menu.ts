import { Component } from '@angular/core';
import {Home} from '../../pages/home/home';
/**
 * Generated class for the Menu component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})
export class Menu {

  text: string;
 private rootPage;
  private homePage;
  constructor() {
    console.log('Hello Menu Component');
    this.text = 'Hello World';
     this.rootPage = Home;
     this.homePage = Home;
  }

}
