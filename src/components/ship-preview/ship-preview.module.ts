import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ShipPreview } from './ship-preview';

@NgModule({
  declarations: [
    ShipPreview,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ShipPreview
  ]
})
export class ShipPreviewModule {}
