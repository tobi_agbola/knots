import { Component } from '@angular/core';
import { NavController,ViewController, NavParams } from 'ionic-angular';
import {ShipInformation} from '../../pages/ship-information/ship-information';

/**
 * Generated class for the ShipPreview component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'ship-preview',
  templateUrl: 'ship-preview.html'
})
export class ShipPreview {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController,public params: NavParams) {
    console.log('Hello ShipPreview Component');
    this.setVariables();
  }
  public shipName;
  public ShipID;
  public Flag;
  public IMO;
  public ShipType;

  dismissmodal() {

    this.viewCtrl.dismiss();

  }
  setVariables(){
    this.shipName = this.params.get('Shipname');
    this.ShipID = this.params.get('ShipID');
    this.Flag = this.params.get('Flag');
    this.IMO = this.params.get('IMO');
    this.ShipType = this.params.get('ShipType');

  }

  GetShipInformation(Ship_Id){

    // if(sessionStorage.getItem("UID")){
      this.navCtrl.push(ShipInformation,{
        "Ship_Id" : Ship_Id
      });
    // }else{
    //   let alert = this.alertCtrl.create({
    //     title: 'Login to access this feature',
    //     buttons: ['Dismiss']
    //   });
    //   alert.present();
    // }
  }

}
