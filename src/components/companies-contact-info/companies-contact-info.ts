import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the CompaniesContactInfo component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'companies-contact-info',
  templateUrl: 'companies-contact-info.html'
})
export class CompaniesContactInfo {
  public companyName;
  public companyEmail;
  public companyPhoneNo;
  public companyAddress;
  public companyDescription;
  public companyLogo;
  constructor( public viewCtrl: ViewController, public params: NavParams ) {
    this.setCompanyDetails();
  }

  dismissmodal() {

    this.viewCtrl.dismiss();

  }

  setCompanyDetails(){

    this.companyName = this.params.get("companyName");
    this.companyEmail = this.params.get("companyEmail");
    this.companyPhoneNo = this.params.get("companyNumber");
    this.companyAddress = this.params.get("companyAddress");
    this.companyDescription = this.params.get("companyDescription");
    this.companyLogo = this.params.get("companyLogo");


  }

}
