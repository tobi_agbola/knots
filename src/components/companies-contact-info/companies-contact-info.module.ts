import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CompaniesContactInfo } from './companies-contact-info';

@NgModule({
  declarations: [
    CompaniesContactInfo,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    CompaniesContactInfo
  ]
})
export class CompaniesContactInfoModule {}
