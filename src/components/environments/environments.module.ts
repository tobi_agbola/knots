import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Environments } from './environments';

@NgModule({
  declarations: [
    Environments,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    Environments
  ]
})
export class EnvironmentsModule {}
