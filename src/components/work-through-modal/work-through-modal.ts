import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
/**
 * Generated class for the WorkThroughModal component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'work-through-modal',
  templateUrl: 'work-through-modal.html'
})
export class WorkThroughModal {

  public slide1 = false;
  public slide2 = true;
  public slide3 = true;

  constructor(public viewCtrl: ViewController) {

  }
  dismissmodal() {

    this.viewCtrl.dismiss();

  }
  changeSlideRight(){

    if(!this.slide1){
      this.slide2 = false;
      this.slide1 = true;
      this.slide3 = true;
    }
    if(!this.slide2){
      this.slide3 = false;
      this.slide1 = true;
      this.slide3 = true;
    }
    if(!this.slide3){
      this.dismissmodal();
    }

  }
  changeSlideLeft(){

    if(!this.slide2){
      this.slide2 =true;
      this.slide1 = false;
      this.slide3 = true;
    }
    if(!this.slide3){
      this.slide2 = false;
      this.slide1 = true;
      this.slide3 = true;
    }

  }

  slideIcon1(){

    this.slide1 = false;
    this.slide2 = true;
    this.slide3 = true;

  }

  slideIcon2(){

    this.slide1 = true;
    this.slide2 = false;
    this.slide3 = true;

  }

  slideIcon3(){

    this.slide1 = true;
    this.slide2 = true;
    this.slide3 = false;

  }


}
