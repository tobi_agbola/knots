import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { WorkThroughModal } from './work-through-modal';

@NgModule({
  declarations: [
    WorkThroughModal,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    WorkThroughModal
  ]
})
export class WorkThroughModalModule {}
