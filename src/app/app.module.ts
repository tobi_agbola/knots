import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { Home } from '../pages/home/home';
import { Login } from '../pages/login/login';
import { Profile } from '../pages/profile/profile';
import { Packages } from '../pages/packages/packages';
import { Register } from '../pages/register/register';
import { Settings } from '../pages/settings/settings';
import { Feedback } from '../pages/feedback/feedback';
import { ShipInformation } from '../pages/ship-information/ship-information';
import { ForgotPassword } from '../pages/forgot-password/forgot-password';
import { MyFleet } from '../pages/my-fleet/my-fleet';
import { ShipsNearMe } from '../pages/ships-near-me/ships-near-me';
import { Mydocuments } from '../pages/mydocuments/mydocuments';
import { Accounts } from '../pages/accounts/accounts';
import { OtherMarineServices } from '../pages/other-marine-services/other-marine-services';
import { ShipToshipCompanies } from '../pages/ship-toship-companies/ship-toship-companies';
import { ShippingAgencies } from '../pages/shipping-agencies/shipping-agencies';
import { ShippingOrders } from '../pages/shipping-orders/shipping-orders';

import { ShipPreview } from '../components/ship-preview/ship-preview';
import { Imageslide } from '../components/imageslide/imageslide';
import { AddOtherDocument } from '../components/add-other-document/add-other-document';
import { ShipInformationPayInfo } from '../components/ship-information-pay-info/ship-information-pay-info';
import { CompaniesContactInfo } from '../components/companies-contact-info/companies-contact-info';
import { CardChoiceComponent } from '../components/card-choice-component/card-choice-component';
import { WorkThroughModal } from '../components/work-through-modal/work-through-modal';
import { ShipSearchComponent } from '../components/ship-search-component/ship-search-component';

import { Uploader } from '../providers/uploader';
import {} from '@types/googlemaps';
import {Geolocation} from '@ionic-native/geolocation';
import { SimulateShips } from '../providers/simulate-ships';
import { MapGeocoder } from '../providers/map-geocoder';
import { UserStatus } from '../providers/user-status';
import { Mailsender} from '../providers/mailsender';
import { PaystackApiCalls } from '../providers/paystack-api-calls';
import { SubscribeUsers } from '../providers/subscribe-users';
import { FirebaseDatabaseCalls } from '../providers/firebase-database-calls';
import { Camera } from '@ionic-native/camera';
import { Screenshot } from '@ionic-native/screenshot';
import { Angular4PaystackModule } from 'angular4-paystack';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import 'firebase/storage';
  this.environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCJu-H2jdbj2-uAPcRySgFtIKUFWdANvww',
    authDomain: 'knots-c3492.firebaseapp.com',
    databaseURL: 'https://knots-c3492.firebaseio.com/',
    projectId: 'knots-c3492',
    storageBucket: 'gs://knots-c3492.appspot.com',
    messagingSenderId: '642835499812'
          }
};
@NgModule({
  declarations: [
    MyApp,
    Login,
    ShipInformation,
    Home,
    Packages,
    Register,
    ForgotPassword,
    Settings,
    Profile,
    Feedback,
    Imageslide,
    ShipsNearMe,
    MyFleet,
    Mydocuments,
    Accounts,
    WorkThroughModal,
    OtherMarineServices,
    ShipToshipCompanies,
    ShippingAgencies,
    AddOtherDocument,
    ShipInformationPayInfo,
    CompaniesContactInfo,
    CardChoiceComponent,
    ShipSearchComponent,
    ShippingOrders,
    ShipPreview,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(this.environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    Angular4PaystackModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents: [
    MyApp,
    Login,
    ShipInformation,
    Home,
    Packages,
    Register,
    ForgotPassword,
    Settings,
    Profile,
    Feedback,
    Imageslide,
    ShipsNearMe,
    MyFleet,
    Mydocuments,
    Accounts,
    WorkThroughModal,
    OtherMarineServices,
    ShipToshipCompanies,
    ShippingAgencies,
    AddOtherDocument,
    ShipInformationPayInfo,
    CompaniesContactInfo,
    CardChoiceComponent,
    ShipSearchComponent,
    ShippingOrders,
    ShipPreview,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    GoogleAnalytics,
    Screenshot,
    SimulateShips,
    MapGeocoder,
    Mailsender,
    NativeGeocoder,
    Uploader,
    PhotoLibrary,
    UserStatus,
    Geolocation,
    PaystackApiCalls,
    FirebaseDatabaseCalls,
    SubscribeUsers,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}


