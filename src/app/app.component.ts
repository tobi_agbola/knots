import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Login } from '../pages/login/login';
import { Home } from '../pages/home/home';
import { Register } from '../pages/register/register';
import { ForgotPassword} from '../pages/forgot-password/forgot-password';
import { Profile } from '../pages/profile/profile';
import { Settings } from '../pages/settings/settings';
import { Feedback } from '../pages/feedback/feedback';
import { MyFleet } from '../pages/my-fleet/my-fleet';
import { ShipsNearMe } from '../pages/ships-near-me/ships-near-me';
import { Mydocuments } from '../pages/mydocuments/mydocuments';
import { Accounts } from '../pages/accounts/accounts';
import { OtherMarineServices } from '../pages/other-marine-services/other-marine-services';
import { Packages } from '../pages/packages/packages';
import { ShippingOrders } from '../pages/shipping-orders/shipping-orders';

import { AngularFireAuth } from 'angularfire2/auth';
import { Screenshot } from '@ionic-native/screenshot';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
@Component({
  templateUrl: 'app.html' 
})
export class MyApp {
  rootPage:any = Home;
  Loggedin=false;
@ViewChild(Nav)nav:Nav;
  constructor(platform: Platform, statusBar: StatusBar,private screenshot: Screenshot, splashScreen: SplashScreen, public menuCtrl: MenuController, public afAuth: AngularFireAuth,private ga: GoogleAnalytics) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.styleDefault();

      setTimeout(() => {

        splashScreen.hide();

      }, 100);

    });

    this.SidebarStatus();

    this.GoogleTracker();

  }
  gotoSettings(){

    this.nav.push(Settings);

    this.menuCtrl.close();
  }
  gotoFeedback(){

    this.nav.push(Feedback);

    this.menuCtrl.close();

  }
  gotoProfile(){

    this.nav.push(Profile);

    this.menuCtrl.close();

  }
  gotoDocuments(){

    this.nav.push(Mydocuments);

    this.menuCtrl.close();

  }
  gotoCards(){

    this.nav.push(Accounts);

    this.menuCtrl.close();

  }
  gotoHome(Page){

    this.nav.root();

    this.menuCtrl.close();

  }
  GuestGotToHome(){

    this.nav.popToRoot();

    this.menuCtrl.close();

  }
  logOut(){
    var that = this;
    this.afAuth.auth.signOut().then(function(data){
      console.log(data);
      sessionStorage.clear();

    //sessionStorage.setItem('splash', 'true'); //set splashScreen

    that.nav.popToRoot();

    that.nav.push(Login);

    that.menuCtrl.close();

    }).catch(function(err){
      console.log(err);
    })


  }
  gotoForgotPassword(){

    this.nav.push(ForgotPassword);

    this.menuCtrl.close();
  }
  gotoRegister(){

    this.nav.push(Register);

    this.menuCtrl.close();

  }
  gotoOtherMarineServices(){

    this.nav.push(OtherMarineServices);

    this.menuCtrl.close();

  }
  gotoScreenshot(){

    this.menuCtrl.close();

    this.takeScreenshot();

  }
takeScreenshot(){

  this.screenshot.save('jpg',50).then(res=>{

    console.log('done');

    console.log(res);

  }).catch(err=>{console.log(err)})
}
  ShipsNearMe(){

    this.nav.push(ShipsNearMe);

    this.menuCtrl.close();

  }
  gotoLogin(){

    this.nav.push(Login);

    this.menuCtrl.close();

    }
  myFleet(){

    this.nav.push(MyFleet);

    this.menuCtrl.close();

  }
  gotoPackages(){

    this.nav.push(Packages);

    this.menuCtrl.close();

  }
  gotoShippingOrders(){

    this.nav.push(ShippingOrders);

    this.menuCtrl.close();

  }
  SidebarStatus(){
    if(sessionStorage.getItem("UID")==null) {

      this.Loggedin=false;

    }else{

      this.Loggedin=true;

    }
  }

  GoogleTracker() {

    this.ga.startTrackerWithId('UA-113802115-1').then(() => {

      console.log('Google analytics is ready now');

      this.ga.trackView('test');

      alert("test");

      if(this.afAuth.auth.currentUser.uid.toString()) {

        this.ga.setUserId(this.afAuth.auth.currentUser.uid.toString());

      }else {

        this.ga.setUserId('guest');

      }
      // Tracker is ready
      // You can now track pages or set additional information such as AppVersion or UserId
  }).catch(e => console.log('Error starting GoogleAnalytics', e));
  }
  popMenuCtrl(){

    this.menuCtrl.close();

  }
}

